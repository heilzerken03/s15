console.log('Hello, World!')

// SECTION 1
let firstName = 'Ken'
console.log("First Name: " + firstName)
let lastName = 'Simborio'
console.log("Last Name: " + lastName)

let age = 30
console.log("Age: " + age)

let hobbies = ["Reading", "Cooking", "Coding"]
console.log("Hobbies: ");
console.log(hobbies);

let workaddress = {
	houseNumber: 3400,
	street: 'Warner Blvd',
	city: 'Burbank',
	state: 'California',
}
console.log("Work Address: ");
console.log(workaddress);

// SECTION 2 - Debugging Practice:

let fullName = " Steve Rogers";
console.log("My full name is" + fullName);

let currentAge = 40
console.log("My current age is: " + currentAge);

let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

let profile = {
		userName: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,
	}
console.log("My Full Profile: ")
console.log(profile);

let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestFriend);

	const lastLocation = "Arctic Ocean";
	lastLocation[0] = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);

